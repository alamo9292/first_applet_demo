// pages/cart/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    address: {},
    cartList: [],
    totalPrice: 0,
    totalCount:0,
    checkAll: false
  },
  // getSetting(){
  //   wx.getSetting({
  //     success:result=>{
  //       console.log(result);
  //     }
  //   })
  // },
  // openSetting(){
  //   wx.getSetting({
  //     success:result=>{
  //       console.log(result);
  //     }
  //   })
  // }
  // ,
  onShow(){
    this.setData({
      address: wx.getStorageSync('address') || {},
      cartList: wx.getStorageSync('cartList') || []
    })

    // 计算总价
    this.computedCartData()
  },
  changeCheckAll(){
    let {cartList,checkAll} = this.data

    checkAll = !checkAll

    // 购物车列表的选中状态和全选保持一致
    cartList.forEach(v=>{
      v.goods_selected = checkAll
    })

    this.setData({
      cartList,
      checkAll
    })
    // 计算总价
    this.computedCartData()
  },
  getAddressHandle(){
    wx.getSetting({ //这里无论是取消还是确认授权都是success
      success:result=>{
        //authSetting是微信内置的一个API，用于获取用户设置信息
        if(result.authSetting["scope.address"] === false){
          wx.openSetting({
            success:result=>{
              this.getAddress()
            }
          })
        }else{
          this.getAddress()
        }
      }
    })
  },
  computedCartData(){
    let {cartList} = this.data
    let totalPrice = 0
    let totalCount = 0
    cartList.forEach(v=>{
      if(v.goods_selected){
         // 总金额
        totalPrice += v.goods_price * v.goods_count;
         // 选中件数
        totalCount++
      }
    })

    this.setData({
      totalPrice,
      totalCount,
      checkAll:cartList.length === totalCount,
      cartList
    })

    wx.setStorageSync('cartList', cartList);
  },
  getAddress(){
 // 小程序提供的获取收货地址的API
 wx.chooseAddress({
  success:result=>{
    const {cityName,countyName,detailInfo,nationalCode,postalCode,provinceName,telNumber,userName} = result
    const address = {
      cityName,
      countyName,
      detailInfo,
      nationalCode,
      postalCode,
      provinceName,
      telNumber,
      userName,
      // 额外拼接了一个新的字符串
      addressDetail:`${provinceName}${cityName}${countyName}${detailInfo}`
    }
    wx.setStorageSync('address',address)
    this.setData({
      address
    })
  },
  fail:err=>{
    wx.showToast({
      title:'你取消了地址选择',
      icon:'none'
    })
  }
 })
  },
  onLoad(options) {
    
  },
  // 计数器加减号事件
  changeCount(event){
    let {index,number} = event.currentTarget.dataset;
    let {cartList} = this.data
    if(number===-1 && cartList[index].goods_count===1){
      // 模态提示框，有两个按钮
      wx.showModal({
        title: '是否删除商品',
        showCancel: true,
        cancelText: '取消',
        cancelColor: '#000000',
        confirmText: '确定',
        confirmColor: '#3CC51F',
        success:result=>{
          if(result.confirm){
            cartList.splice(index,1)
            this.computedCartData()
          }else{
            console.log('点了取消');
          }
        }
      })
    }else{
      cartList[index].goods_count += number
      this.computedCartData()
    }
  },
   // 列表项的选择按钮点击
   changeCheck(event){
     let {index} =  event.currentTarget.dataset
     let {cartList} = this.data
     
     cartList[index].goods_selected = !cartList[index].goods_selected

     this.computedCartData()

   }
})