const app = getApp()
import regeneratorRuntime from '../../lib/runtime/runtime'
// 创建APP实例，里面有封装好的axios和引入async|await的库
Page({

  /**
   * 页面的初始数据
   */
  data: {
    details:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
 async onLoad(options) {
      const res = await app.myAxios({
        url:'goods/detail',
        data:options
      })
      // console.log(details);
      this.setData({
        details:res
      })
      console.log(this.data.details);

      const { platform } =  wx.getSystemInfoSync();
    // console.log(system);
    if(platform === 'ios'){
      // .+?   匹配多个任意字符，并阻止贪婪模式(最近一个webp结束)
      res.goods_introduce = res.goods_introduce.replace(/\?.+?webp/g,'')
    }
  },
  previewBigImage(event){
    const {src} = event.currentTarget.dataset;
    const newSrcs = this.data.details.pics.map(v=> v.pics_big)
    wx.previewImage({
      urls:newSrcs,
      current:src
    })
  },
  goToCart(){
    wx.switchTab({
      url:'/pages/cart/index'
    })
    this.addToCart()
  },
  addToCart(){
    const {goods_name,goods_price,goods_id,goods_small_logo} = this.data.details
    const cartList = wx.getStorageSync('cartList') || []
    const index = cartList.findIndex(v=>{
      return v.goods_id === goods_id;
    })
    if(index == -1){
      cartList.push({
        goods_name,
        goods_price,
        goods_id,
        goods_small_logo,
        goods_count : 1,
        goods_selected:true
      })
    }else{
      cartList[index].goods_count++
    }

    wx.setStorageSync('cartList',cartList)

    wx.showToast({
      title:'添加成功囖',
      icon:'success',
      duration:500,
      mask:true
    })
  }


})