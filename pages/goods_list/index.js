const app = getApp()
import regeneratorRuntime from '../../lib/runtime/runtime'
// 创建APP实例，里面有封装好的axios和引入async|await的库
Page({

  data: {
    productList: [],
    activeIndex: 0,
    titleBar: [{
        name: '综合',
        id: '1'
      },
      {
        name: '销量',
        id: '2'
      },
      {
        name: '价格',
        id: '3'
      }

    ],
    pagenum: 1,
    pagesize: 10,
    total: 0,
  },

  selectIndex(event) {
    const {
      index
    } = event.currentTarget.dataset
    this.setData({
      activeIndex: index
    })
  },
  async getList() {
    const productList = await app.myAxios({
      url: 'goods/search',
      data: {
        ...this.options,
        pagenum: this.data.pagenum,
        pagesize: this.data.pagesize
      }
    })
    console.log(productList)
    this.setData({
      productList: [...productList.goods, ...this.data.productList].map(v=>{
        return {
          ...v,
          goods_price01: v.goods_price.toFixed(2).split('.')[0],
          goods_price02: v.goods_price.toFixed(2).split('.')[1]
        }
      }),
      total: productList.total
    })
  },
  onLoad(options) {
    this.getList()
  },
  //下拉刷新触发的函数
  onPullDownRefresh() {
    this.setData({
      productList: [],
      pagenum: 1
    })

    this.onLoad(this.option)
  },
  //上拉触发的函数
  onReachBottom() {
    let {
      pagenum,
      pagesize,
      total
    } = this.data
    if (pagenum < Math.ceil(total / pagesize)) {
      this.setData({
        pagenum: ++pagenum
      })
      this.getList()
    } else {
      wx.showToast({
        title: '没数据拉咯····',
        duration: 1000,
        icon: 'success'
      })
    }
  }
})