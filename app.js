//app.js
import {myAxios} from './utils/myAxios'
App({
  myAxios,
  globalData: {
    userInfo: null
  }
})