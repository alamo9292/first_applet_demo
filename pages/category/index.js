Page({
  data: {
    cateArr: wx.getStorageSync('cateArr') || [],
    cateRight:wx.getStorageSync('cateRight') || [],
    activeIndex:0,
    rightTop:0
  },
  changeIndex(event){
    const {index} = event.currentTarget.dataset
    this.setData({
      activeIndex:index,
      cateRight:this.data.cateArr[index].children,
      rightTop:0
    })
    // wx.pageScrollTo({
     
    // })
  },
  onLoad() {
    // console.log(this.data.cateArr)
    // 当长度不为0的时候数据就不会获取。第一个cateRight就无法赋值，所以要多加一个逻辑或来
    // 判断第一个cateRight的长度是否为0
    if (this.data.cateArr.length === 0 || this.data.cateRight.length === 0 ) {
      wx.request({
        url: 'https://api.zbztb.cn/api/public/v1/categories',
        success: res => {
          // console.log(res)
          const cateArr = res.data.message
          const cateRight = cateArr[0].children
          wx.setStorageSync('cateArr', cateArr)
          wx.setStorageSync('cateRight',cateRight)
          this.setData({
            cateArr: cateArr,
            cateRight:cateRight
          })
        }
      })
    }
  }
})