const baseURL = 'https://api.zbztb.cn/api/public/v1/'


export const myAxios = (params)=>{
    wx.showLoading({
        title:'Loading····'
    })
   return new Promise((resolve,reject)=>{
        wx.request({
            ...params,
            url:baseURL+params.url,
            
            success:result=>{
                resolve(result.data.message)
            },
            fail:error=>{
                reject(error)
            },
            complete:()=>{
                wx.hideLoading()
                //停止下拉刷新
                wx.stopPullDownRefresh()
            }
        })
    })
}